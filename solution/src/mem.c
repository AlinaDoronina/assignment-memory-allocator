#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

struct region alloc_region  ( void const * addr, size_t query ) {
  if (!addr) return REGION_INVALID;

  block_size block_sz = (block_size) {query}; size_t length = region_actual_size(block_sz.bytes);

  void* addr_region = map_pages(addr, length, MAP_FIXED_NOREPLACE);
  if (addr_region==MAP_FAILED) {
    addr_region = map_pages(addr, length, 0);
    if (addr_region==MAP_FAILED) return REGION_INVALID;
  }

  block_init(addr_region, (block_size){length}, NULL);
  struct region region = {addr_region, length, (addr==addr_region)? true: false};
  
  return region;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  block_capacity cap = (block_capacity) {initial};
  const struct region region = alloc_region( HEAP_START, size_from_capacity(cap).bytes);
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {

  if (!block->is_free || !block_is_big_enough(query, block)) return false;
  else {

    if (!block_splittable(block, query)) return true;

    block_capacity size = (block_capacity) {block->capacity.bytes-query-offsetof(struct block_header, contents)};
    block_init((uint8_t*)block->contents+0x1*query, size_from_capacity(size), block->next);

    block->next = (struct block_header*)((uint8_t*)block->contents+0x1*query); block->capacity = (block_capacity) {query};
    return true;

  }
}

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (!block->next) return false;
  else {

    if (mergeable(block, block->next)) {
      block->capacity = (block_capacity) {block->capacity.bytes+size_from_capacity(block->next->capacity).bytes};
      block->next = block->next->next;
      return true;
    } 
    
    return false;
  }
}


struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

static bool block_is_good(struct block_header* block, size_t query) {
  return block->is_free && block_is_big_enough(query, block);
}


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  if (block==NULL) return (struct block_search_result) {BSR_CORRUPTED, block};
  
  while (block->next!=NULL) {
    if (block_is_good(block, sz)) return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, block};
    if (!try_merge_with_next(block)) block = block->next;
  }

  if (block_is_good(block, sz)) return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, block};

  return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, block};
}

static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type==BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(result.block, query);
    result.block->is_free = false;
  }
  return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (!last) return NULL;

  void* addr = block_after(last); block_capacity new_region_capacity = (block_capacity) {query};

  struct region new_region = alloc_region(addr, size_from_capacity(new_region_capacity).bytes);
  if (!new_region.addr) return NULL;

  last->next = (struct block_header*) new_region.addr;

  return (struct block_header*) new_region.addr;
}

static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if (query < BLOCK_MIN_CAPACITY) query = BLOCK_MIN_CAPACITY;

  while (heap_start->next) {
    if (heap_start->is_free) break;
    heap_start = heap_start->next;
  }

  struct block_search_result result = try_memalloc_existing(query, heap_start);

  if (result.type != BSR_FOUND_GOOD_BLOCK) {
    heap_start->next = grow_heap(heap_start, query);
    result = try_memalloc_existing(query, heap_start);
  }
  return result.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

struct maybe_prev_block {
  bool valid;
  struct block_header* prev_block;
};

static struct maybe_prev_block find_previous_block_header(struct block_header* block) {
  if (block==HEAP_START) return (struct maybe_prev_block) {false, block};

  struct block_header* block_iterable = (struct block_header*) HEAP_START;
  while (block_iterable->next!=NULL) {
    if (block_iterable->next==block && block_after(block_iterable)==block) return (struct maybe_prev_block) {true, block_iterable};
    block_iterable = block_iterable->next;
  }

  return (struct maybe_prev_block) {false, block};
} 

static struct block_header* try_find_previous_block_header(struct block_header* block) {
  struct maybe_prev_block mb_prev_block = find_previous_block_header(block);

  if (mb_prev_block.valid && mb_prev_block.prev_block->is_free) return mb_prev_block.prev_block;
  else return block;
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  header = try_find_previous_block_header(header);

  while (header->next && header->next->is_free) {
    if (!try_merge_with_next(header)) break;
  }
}
