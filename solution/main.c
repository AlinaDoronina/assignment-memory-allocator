#include "src/mem.h"
#include "src/mem_internals.h"
#include "test/test_1.h"
#include "test/test_2.h"
#include "test/test_3.h"
#include "test/test_4.h"
#include "test/test_5.h"

void test() {
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
}

int main() {
    heap_init(REGION_MIN_SIZE-17);//test
    test();
    return 0;
}
