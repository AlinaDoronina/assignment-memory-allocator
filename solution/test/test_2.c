#include "../src/mem.h"
#include "test_2.h"

static uint8_t* init_and_debug_block(size_t query) {
    void* addr =_malloc(query);
    if (addr!=NULL) return (uint8_t*) addr;
    else return NULL;
}

static void freeing_one_block_from_several_allocated_ones_handler() {
    printf("TEST 2: freeing one block from several allocated ones.\n");
    debug_heap(stdout, HEAP_START);

    uint8_t* addr = init_and_debug_block(1);
    if (addr==NULL) printf("TEST2 failed.");

    uint8_t* addr2 = init_and_debug_block(30);
    if (addr2==NULL) printf("TEST2 failed.");
    debug_heap(stdout, HEAP_START);

    _free(addr2);
    debug_heap(stdout, HEAP_START);

    uint8_t* addr3 = init_and_debug_block(70);
    if (addr3==NULL) printf("TEST2 failed.");
    debug_heap(stdout, HEAP_START);

    //prepairing heap for the next tests
    _free(addr); _free(addr3); printf("\n");
}

void test_2() {
    freeing_one_block_from_several_allocated_ones_handler();
}
