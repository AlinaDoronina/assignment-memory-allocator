#include "../src/mem.h"
#include "test_1.h"

static uint8_t* init_and_debug_block(size_t query) {
    void* addr =_malloc(query);
    if (addr!=NULL) return (uint8_t*) addr;
    else return NULL;
}

static void normal_successful_memory_allocation_handler() {
    printf("TEST 1: normal successful memory allocation.\n");
    debug_heap(stdout, HEAP_START);

    uint8_t* addr = init_and_debug_block(1);
    if (addr==NULL) printf("TEST1 failed.");
    debug_heap(stdout, HEAP_START);

    //prepairing heap for the next tests
    _free(addr); printf("\n");
}

void test_1() {
    normal_successful_memory_allocation_handler();
}
